const { token, Comment, Token, String, Parenthesis } = require('./token');
const { parse, ASTNode, Quote, Cond, Define, Lambda, Apply } = require('./parse');
const { compileAST } = require('./compile');

function lambdaWrap(s) {
    return `${s}`
}
function beginWrap(s) {
    return `(function(){\nreturn ${s}})()`
}
function beginWrapEnd(s) {
    return `(function(){\n${s}})()`
}
test('quote', () => {
    expect(compileAST(parse(token("(quote a)")))).toStrictEqual(
        (`new SSymbol("a")`)
    );
    expect(compileAST(parse(token(
        "(eval2 '(car (cons (1 1))) the-global-environment)")))).toStrictEqual(
            (`(eval2)([new SSymbol("car"),[new SSymbol("cons"),[1,1]]],the__global__environment)`)
        );
});
test('quote sugar', () => {
    expect(compileAST(parse(token("'a")))).toStrictEqual(
        (`new SSymbol("a")`)
    );
    expect(compileAST(parse(token("' a")))).toStrictEqual(
        (`new SSymbol("a")`)
    );
});
test('quote list', () => {
    expect(compileAST(parse(token("(quote (a 2))")))).toStrictEqual(
        (`[new SSymbol("a"),2]`)
    );
    expect(compileAST(parse(token("(car '(+ 1))")))).toStrictEqual(
        (`(car)([new SSymbol("+"),1])`)
    );
});
test('cond', () => {
    expect(compileAST(parse(token("(cond (a b) (c d))")))).toStrictEqual(
        (`(function(){if(a){
return b} else if(c){
return d}})()`
        ));
});
test('if', () => {
    expect(compileAST(parse(token("(if c a b)")))).toStrictEqual(
        (`(function(){if(c){
return a} else if(true){
return b}})()`
        ));
});

test('define', () => {
    expect(compileAST(parse(token("(define a 42)")))).toStrictEqual(
        ("var a;\na=42")
    );
});
test('begin', () => {
    expect(compileAST(parse(token("(define a 42) a")))).toStrictEqual(
        ("var a;\na=42;\na")
    );
});
// test('define context', () => {
//     let context = {}
//     expect(compileAST(parse(token("(define a 42)")), context)).toStrictEqual(
//         ("var a;\na=42;\ncontext.a=a")
//     );
// });
test('lambda', () => {
    expect(compileAST(parse(token("(lambda (x) x)")))).toStrictEqual(
        (`(function(x){\n${lambdaWrap('return x')}})`)
    );
});
test('apply', () => {
    expect(compileAST(parse(token("(a b c)")))).toStrictEqual(
        (`(a)(b,c)`)
    );
    expect(compileAST(parse(token("(a )")))).toStrictEqual(
        (`(a)()`)
    );
});
test('let', () => {
    expect(compileAST(parse(token("(let ((a 1)) (+ a 2))")))).toStrictEqual(
        (`((function(a){\n${lambdaWrap('return (a+2)')}}))(1)`)
    );
});
test('+', () => {
    expect(compileAST(parse(token("(+ a b c)")))).toStrictEqual(
        (`(a+b+c)`)
    );
});
test('<', () => {
    expect(compileAST(parse(token("(< a b)")))).toStrictEqual(
        (`(a<b)`)
    );
});
test('complicate', () => {
    expect(compileAST(parse(token(`(/ (+ x y) 2)`)))).toStrictEqual(
        (`((x+y)/2)`)
    );
});
test('safe name', () => {
    expect(compileAST(parse(token(`(a-b c)`)))).toStrictEqual(
        (`(a__b)(c)`)
    );
});
test('sqrt', () => {
    expect(compileAST(parse(token(`
    (define (average x y)
  (/ (+ x y) 2))`)))).toStrictEqual(
        (`var average;
average=(function(x,y){\n${lambdaWrap('return ((x+y)/2)')}})`)
    );
});
test('fact', () => {
    expect(compileAST(parse(token(`
    (define (fact n)
  (if (= n 1) 1 (* n (fact(- n 1)))))`)))).toStrictEqual(
        (`var fact;
fact=(function(n){\n${lambdaWrap('if((n==1)){\nreturn 1} else if(true){\nreturn (n*(fact)((n-1)))}')}})`)
    );
});
test('fact tail', () => {
    expect(compileAST(parse(token(`
    (define (fact n a)
  (if (= n 1) 
  a 
  (fact (- n 1) (* n a))))`)))).toStrictEqual(
        (`var fact;
fact=(function(n,a){\nbegin__:while(true){if((n==1)){\nreturn a} else if(true){
/*tail recur*/[n,a]=[(n-1),(n*a)];\ncontinue begin__};break;}})`)
    );
});