# jsp

#### 介绍

compile scheme(a dialect of lisp) to js

支持的语法：

- define
- lambda
- cond
- if
- let
- quote（以及'语法糖）
- begin

支持最基本的尾递归：

```ss
(define (fact n a)
  (if (= n 1) 
    a 
    (fact (- n 1) (* n a))))
```

will compile:

```js
fact = (function (n, a) {
    begin__: while (true) {
        if ((n == 1)) {
            return a
        } else if (true) {
/*tail recur*/[n, a] = [(n - 1), (n * a)];
            continue begin__
        }; break;
    }
});
```

其他的函数，写在了 buildin.js 中，如下：

- null?
- pair?
- list?
- number?
- string?
- boolean?
- symbol?
- procedure?
- eq?
- eqv?
- equal?
- cons
- car
- cdr
- list
- map
- length
- set-car!
- set-cdr!
- display
- error
- apply
- and
- or
- not
- memq
- load
- cadr
- cdar
- cddr

更亲和js的地方：

- 使用true/false/null 而非 #f #t nil
- 可以使用 == 和 === 做相等的判断

与 r5rs 不同的地方：

1. 标识符里的特殊符号只有 -<>?! 并被转换为普通的JS标识符
2. .运算符依然被保留在生成的JS代码里 (Math.pow 2 3)
3. 没有 点对 字面量(但有内部实现)
4. 不支持case
5. 没有let*和letrec
7. 没有do
9. 没有quosiquote,
10. 没有宏
11. 没有精确数，complex,rational
12. 没有delay,force,eval, call/cc
13. 没有 file相关


#### 使用说明

1.  `node main.js -f src.ss > dist.js` to compile file
1.  `node main.js -i` to open repl
1.  `node main.js -i --load a.ss,b.ss` to open repl with file a and b loaded
1.  `node main.js --run file.ss` to run scheme file

#### 参与贡献

按照以下里程碑贡献代码

1. letrec
2. let compile and tail
3. 宏
4. call/cc

如何做：

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
