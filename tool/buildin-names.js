const fs = require("fs");
// let fs = require("fs-extra");
let path = require("path");

const contentJS = fs.readFileSync(path.join(__dirname, "buildin.js"));
const buildinJS = contentJS.toString();

let lines = buildinJS.split(/\r|\n|\r\n/).filter(line => line !== "")
for (let i = 0; i < lines.length; i++) {
    let line = lines[i]
    let m = line.match(/^function ([a-z]+)/)
    let name
    if (m) {
        name = m[1]
        if ((i - 1) >= 0) {
            let prevLine = lines[i - 1]
            if (prevLine.indexOf("// ") === 0) {
                name = prevLine.slice(3)
            }
        }
        if (name && name.length > 0 && /^[a-z]/.test(name))
            console.log("-", name)
    }
}