let base = [[]]
function mapX(base) {
    let r = []
    for (let e of base) {
        r.push(e.concat('a'), e.concat('d'))
    }
    return r
}
let comb = mapX(mapX(base)).concat(mapX(mapX(mapX(base)))).concat(mapX(mapX(mapX(mapX(base)))))
// console.log(comb)
for (let cmd of comb) {
    console.log("function", "c" + cmd.join("") + "r", "(x){return",
        cmd.map(x => "c" + x + "r(").join(""), "x", cmd.map(x => ")").join(""),
        "}")
}