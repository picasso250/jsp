class Error {
    constructor(name, pos, message = "") {
        this.name = name;
        this.pos = pos;
        this.message = message;
    }
}
exports.Error = Error