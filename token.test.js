const { pass0, pass1, pass2, token, Comment, Token, String, Parenthesis } = require('./token');

// test('comment', () => {
// });
test('comment and string', () => {
    expect(pass1(pass0(";abc\n"))).toStrictEqual([new Comment(";abc", 0), new Token("\n", 4)]);
    expect(pass1(pass0(`2 "hello" 1`))).toStrictEqual([
        new Token("2 ", 0), new String(`"hello"`, 2), new Token(" 1", 9)]);
    expect(pass1(pass0(`;2 \n"hello" 1`))).toStrictEqual([
        new Comment(";2 ", 0), new Token("\n", 3), new String(`"hello"`, 4), new Token(" 1", 11)]);
    expect(pass1(pass0(`";;;hello" 1`))).toStrictEqual([
        new String(`";;;hello"`, 0), new Token(" 1", 10)]);
});
test('token', () => {
    expect(pass2(pass1(pass0(`(ab c)`)))).toStrictEqual([
        new Parenthesis("(", 0), new Token("ab", 1), new Token("c", 4), new Parenthesis(")", 5)]);
    expect(pass2(pass1(pass0(`(ab c);a\n`)))).toStrictEqual([
        new Parenthesis("(", 0), new Token("ab", 1), new Token("c", 4), new Parenthesis(")", 5),
        new Comment(";a", 6)
    ]);
    expect(token(`("ab" c);a\n`)).toStrictEqual([
        new Parenthesis("(", 0), new String(`"ab"`, 1), new Token("c", 6), new Parenthesis(")", 7),
        new Comment(";a", 8)
    ]);
    expect(token(`(quote a)`)).toStrictEqual([
        new Parenthesis("(", 0),
        new Token("quote", 1), new Token("a", 7),
        new Parenthesis(")", 8)
    ]);
});
