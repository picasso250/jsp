const { token, Comment, Token, String, Parenthesis } = require('./token');
const { pass0, pass1, ASTNode, Quote, Cond, Define, Lambda, Apply, Begin } = require('./parse');

function topLevelQuote(ast) {
    return new ASTNode([
        new Parenthesis("(", 0),
        new Token("begin", 0),
        ast,
        new Parenthesis(")", 0)
    ], 0)
}
test('ast', () => {

    expect(pass0(token("(a b c)"))).toStrictEqual(topLevelQuote(
        new ASTNode([
            new Parenthesis("(", 0),
            new Token("a", 1), new Token("b", 3), new Token("c", 5), new Parenthesis(")", 6)
        ], 0)));
    expect(pass0(token("(quote a)"))).toStrictEqual(topLevelQuote(
        new ASTNode([
            new Parenthesis("(", 0),
            new Token("quote", 1), new Token("a", 7),
            new Parenthesis(")", 8)
        ], 0)));
});
test('comment', () => {
    expect(pass0(token("(a b c);a\n"))).toStrictEqual(topLevelQuote(
        new ASTNode([
            new Parenthesis("(", 0),
            new Token("a", 1), new Token("b", 3), new Token("c", 5), new Parenthesis(")", 6)
        ], 0)));
});
test('quote', () => {
    const tks = token("(quote a)")
    const ast = pass0(tks)
    expect(pass1(ast)).toStrictEqual(new Begin(0,
        [new Quote(0, new Token("a", 7))]))
});
test(`quote'`, () => {
    const tks = token("(s ' b)")
    const ast = pass0(tks)
    expect(pass1(ast)).toStrictEqual(new Begin(0, [
        new Apply(0, new Token("s", 1), [
            new Quote(3, new Token("b", 5))])]))
});
test('cond', () => {
    const tks = token("(cond (a b) (c d))")
    const ast = pass0(tks)
    expect(pass1(ast)).toStrictEqual(new Begin(0, [
        new Cond(0, [
            [new Token("a", 7), new Token("b", 9)],
            [new Token("c", 13), new Token("d", 15)],
        ])]))
});
test('define', () => {
    const tks = token("(define a 1)")
    const ast = pass0(tks)
    expect(pass1(ast)).toStrictEqual(new Begin(0, [
        new Define(0, "a", new Token("1", 10))]))
});
test('lambda', () => {
    const tks = token("(lambda (a) 1)")
    const ast = pass0(tks)
    expect(pass1(ast)).toStrictEqual(new Begin(0, [
        new Lambda(0, ["a"], [new Token("1", 12)])]))
});
test('apply', () => {
    const tks = token("(a b)")
    const ast = pass0(tks)
    expect(pass1(ast)).toStrictEqual(new Begin(0, [
        new Apply(0, new Token("a", 1), [new Token("b", 3)])]))
});
test('define lambda', () => {
    const tks = token("(define id (lambda (x) x))")
    const ast = pass0(tks)
    expect(pass1(ast)).toStrictEqual(new Begin(0, [
        new Define(0, "id",
            new Lambda(11, ["x"], [new Token("x", 23)]))]))
});
