(define (fact n a)
  (if (= n 1) 
    a 
    (fact (- n 1) (* n a))))